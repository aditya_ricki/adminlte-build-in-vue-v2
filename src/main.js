import Vue from 'vue'
import App from './App.vue'
import router from './router'

import Navbar from './components/Navbar.vue'
import Header from './components/Header.vue'
import Sidebar from './components/Sidebar.vue'
import Footer from './components/Footer.vue'

Vue.config.productionTip = false

Vue.component('nav-bar', Navbar)
Vue.component('head-er', Header)
Vue.component('side-bar', Sidebar)
Vue.component('foot-er', Footer)

new Vue({
	router,
	render: h => h(App)
}).$mount('#app')
