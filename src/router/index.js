import Vue from 'vue'
import VueRouter from 'vue-router'

import Dashboard from './../views/Dashboard.vue'
import Login from './../views/Login.vue'
import Register from './../views/Register.vue'

import CrudIndex from './../views/crud/Index.vue'
import CrudCreate from './../views/crud/Create.vue'
import CrudEdit from './../views/crud/Edit.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/crud',
        name: 'CrudIndex',
        component: CrudIndex
    },
    {
        path: '/crud/create',
        name: 'CrudCreate',
        component: CrudCreate
    },
    {
        path: '/crud/:id',
        name: 'CrudEdit',
        component: CrudEdit
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
